# Soft Industry Alliance Test Task

This test framework is built using Java, Serenity, Cucumber, Rest Assured.
The test scripts are written using Java 17.

***
## Setup
1. Install JAVA JDK, https://www.oracle.com/java/technologies/javase-downloads.html we use 17 LTS version.
2. Setup environment variable "JAVA_HOME" and add %JAVA_HOME%\bin folder to system 'Path'.
3. Download the latest Maven, https://maven.apache.org/download.cgi and unpack it to any folder, for example "C:\Program files".
4. Setup environment variable "MAVEN_HOME" and add %MAVEN_HOME%\bin folder to system 'Path'.
5. Run from command line:
```
java -version
```
should return:
```
java version "17.0.8" 2023-07-18 LTS
Java(TM) SE Runtime Environment (build 17.0.8+9-LTS-211)
Java HotSpot(TM) 64-Bit Server VM (build 17.0.8+9-LTS-211, mixed mode, sharing)
```
```
mvn -version
```
should return:
```
Apache Maven 3.9.4 (dfbb324ad4a7c8fb0bf182e6d91b0ae20e3d2dd9)
Maven home: C:\Program Files\apache-maven-3.9.4
Java version: 17.0.8, vendor: Oracle Corporation, runtime: C:\Program Files\Java\jdk-17
Default locale: en_US, platform encoding: Cp1251
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```
6. Download and install IntelliJ IDEA Community Edition https://www.jetbrains.com/idea/download/?section=windows.
7. Open the project with IntelliJ IDEA.

***
## Tests execution
### Locally:
Tests can be easily executed from the IntelliJ IDEA terminal, run the command below:
```
mvn clean verify
```
Wait until execution is finished, html reports can be found in the folder ${path_to_project_folder}/softindustryalliancetask/target/site/serenity/.
Check index.html and serenity-summary.html reports.
### GitLab:
1. Open Build -> Pipelines.
2. Run pipeline.
3. Open test stage details.
![img.png](img.png)
4. Find links to the reports.
![img_1.png](img_1.png)

***
## Writing new tests:
1. Add feature file to the src/test/resources/features folder. Use Cucumber/Gherkin format: https://cucumber.io/docs/gherkin/reference/.
2. Create class for new steps definitions under the folder src/test/java/starter/stepdefinitions https://cucumber.io/docs/gherkin/step-organization/?lang=java.
3. Add test runner class under the src/test/java/starter folder.

***
## What was refactored:
1. Reorganized feature file.
2. Reorganized Step definitions class.
3. Added src/test/java/starter/setup/Requests.java class, to have base requests configurations in the one place to avoid duplications.
4. Added src/test/java/starter/setup/APITestExecutor.java class, may contain general before and after methods.
5. Fixed src/test/java/starter/SearchProductTestRunner.java test runner class.