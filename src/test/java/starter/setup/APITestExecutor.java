package starter.setup;

import lombok.Getter;


/**
 * The class should be used only in API test cases.
 */
@Getter
public abstract class APITestExecutor {

  private final Requests requests = new Requests();
}
