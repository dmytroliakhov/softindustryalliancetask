package starter.setup;

import lombok.Getter;

@Getter
public enum RequestURL {
  GET_PRODUCT_BY_NAME("https://waarkoop-server.herokuapp.com/api/v1/search/demo/{productName}");

  private final String resource;

  RequestURL(final String resource) {
    this.resource = resource;
  }
}
