package starter.setup;

import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;

import java.util.Map;

public class Requests {

  public ValidatableResponse getRequestWithPathParameters(final RequestURL path,
                                                          final Map<String, String> pathParameters,
                                                          final int expectedStatusCode) {
    return SerenityRest.rest()
        .pathParams(pathParameters)
        .contentType("application/json")
        .header("Content-Type", "application/json")
        .when()
        .get(path.getResource())
        .then()
        .statusCode(expectedStatusCode);
  }
}
