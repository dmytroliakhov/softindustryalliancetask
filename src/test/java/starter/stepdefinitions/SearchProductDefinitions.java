package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import starter.api.models.products.Error;
import starter.api.models.products.Product;
import starter.setup.APITestExecutor;

import java.util.Map;

import static java.util.Map.entry;
import static java.util.Map.ofEntries;
import static starter.setup.RequestURL.GET_PRODUCT_BY_NAME;

public class SearchProductDefinitions extends APITestExecutor {
  private Map<String, String> pathParams;
  private Product[] response;
  private Error error;

  @When("User searches product by existing {string} product")
  public void getExistingProduct(String product) {
    pathParams = ofEntries(
        entry("productName", product));
    response = getRequests()
        .getRequestWithPathParameters(GET_PRODUCT_BY_NAME, pathParams, 200)
        .extract()
        .as(Product[].class);
  }

  @When("User searches product by not existing {string} product")
  public void getNotExistingProduct(String product) {
    pathParams = ofEntries(
        entry("productName", product));
    error = getRequests()
        .getRequestWithPathParameters(GET_PRODUCT_BY_NAME, pathParams, 404)
        .extract()
        .as(Error.class);
  }

  @Then("User sees the results for {string} product")
  public void verifyUserSeesResultsForProduct(String product) {
    Assertions
        .assertThat(response.length)
        .as(String.format("Products with %s name were not found.", product))
        .isGreaterThan(0);
  }

  @Then("User does not see the results for {string} product")
  public void verifyUserGetsErrorByNotExistingProduct(String product) {
    SoftAssertions soft = new SoftAssertions();
    soft.assertThat(error.getDetail().getError())
        .as("Error attribute was incorrect")
        .isEqualTo(true);
    soft.assertThat(error.getDetail().getMessage())
        .as("Message attribute was incorrect")
        .isEqualTo("Not found");
    soft.assertThat(error.getDetail().getRequested_item())
        .as("Requested_item attribute was incorrect")
        .isEqualTo(product);
    soft.assertAll();
  }
}
