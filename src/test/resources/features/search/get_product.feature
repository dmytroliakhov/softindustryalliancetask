Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  #Positive
  Scenario Outline:
    When User searches product by existing <product> product
    Then User sees the results for <product> product
    Examples:
      | product |
      | "orange"  |
      | "apple"   |
      | "pasta"   |
      | "cola"    |

  #Negative
  Scenario Outline:
    When User searches product by not existing <product> product
    Then User does not see the results for <product> product
    Examples:
      | product |
      | "orange1" |
      | "car"     |
