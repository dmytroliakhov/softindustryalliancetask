package starter.api.models.products;

import lombok.Getter;

@Getter
public class ErrorDetail {
  private Boolean error;
  private String message;
  private String requested_item;
  private String served_by;
}
