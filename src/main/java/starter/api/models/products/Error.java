package starter.api.models.products;

import lombok.Getter;

@Getter
public class Error {
  private ErrorDetail detail;
}
